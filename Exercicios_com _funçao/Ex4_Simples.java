import java.util.Scanner;

public class Ex4_Simples {
	static Scanner tecla = new Scanner (System.in);
	static double p, l, c;
	static int n;

	public static void main(String[] args) {
		
		System.out.println("Digite a Potência da lâmpada em Watts: ");
		p = tecla.nextDouble();
		System.out.println("Digite o comprimento do cômodo em Metros: ");
		c = tecla.nextDouble();
		System.out.println("Digite a largura do cômodo em Metros: ");
		l = tecla.nextDouble();
		n=(int) calc();
		System.out.println("O numero necessário de lâmpadas é: "+n);

	}
	public static double calc() {
		return ((int) ((l*c)*(18/p)))+1;
		
	}

}
