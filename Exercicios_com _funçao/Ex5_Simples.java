import java.util.Scanner;

public class Ex5_Simples {
	static Scanner tecla = new Scanner(System.in);
	static double c, l, h, l1, l2;
	static int t;
	static final double ca = 1.5;
	
		public static void main(String[] args) {			
			System.out.println("Digite o comprimento: ");
			c = tecla.nextDouble();
			System.out.println("Digite a largura: ");
			l = tecla.nextDouble();
			System.out.println("Digite a altura: ");
			h = tecla.nextDouble();
			l1 = lado1();
			l2 = lado2();
			t= total();
			System.out.println("O cômodo vai necessitar de "+t+" caixas de azuleijo!");		
	}
		public static double lado1() {
			return (h*c)*2;
		}
		public static double lado2() {
			return (h*l)*2;
		}
		public static int total() {
			return (int) (((l1+l2)/ca)+1);
		}

}
