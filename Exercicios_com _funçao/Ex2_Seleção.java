import java.util.Scanner;

public class Ex2_Seleção {
	static Scanner tecla = new Scanner (System.in);
	static double n1, n2, n3, m;
	static final int np = 2;
	static String result;

	public static void main(String[] args) {		
		System.out.println("Digite a nota da Av1: ");
		n1 = tecla.nextDouble();
		System.out.println("Digite a nota da Av2: ");
		n2 = tecla.nextDouble();
		System.out.println("Digite a nota da Av3, caso não tenha feito digite -1: ");
		n3 = tecla.nextDouble();
		m = media();
		result = res(m);
		System.out.println("a nota é "+m+" e o aluno está: "+result);
	}
	public static double media() {
		if(n3 == -1) {
			n3 = 0;
		}
		if(n3>n1) {
			n1 = n3;
		}else
			if(n3>n2) {
				n2 = n3;
			}
		return (n1 + n2) / np;
	}
	public static String res(double m) {
		if(m>=6) {
			return "Aprovado!";
		}else
			if(m<3) {
				return "Reprovado!";
			}else {
				return"Exame final!";
			}
		
	}

}
