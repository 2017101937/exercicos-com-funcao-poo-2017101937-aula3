/*1) Escreva um programa para ler o raio de um círculo, calcular
 *  e escrever a sua área. πR2
 */
import java.util.Scanner;
public class Ex1_simples {
	static double r, a;
	static final double pi = 3.14;
	static Scanner tecla = new Scanner (System.in);
		
	public static void main(String[] args) {
		a=0;
		r=0;
		System.out.println("Digite o Valor do raio: ");
		r = tecla.nextDouble();
		a = area();
		System.out.println("O valor da area é "+a+"m²");
		
	}
	public static double area () {
		return (r*r)*pi;
		
	}

}
