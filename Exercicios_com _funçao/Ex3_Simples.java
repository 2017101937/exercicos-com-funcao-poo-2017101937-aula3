import java.util.Scanner;

public class Ex3_Simples {
	static Scanner tecla = new Scanner (System.in);
	static double f, c;

	public static void main(String[] args) {
		
		System.out.println("Digite a temperatura em Celsius: ");
		c = tecla.nextDouble();
		f= conversão();
		System.out.println("A temperatura em graus celsius é: "+f+"Fº");

	}
	static public double conversão() {
		return (c * 9/5) + 32;
	}

}
